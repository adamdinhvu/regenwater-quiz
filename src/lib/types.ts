
export type Question = {
    question: string
    type: "degree" | "multi"
}

export type DegreeQuestion = Question & {
    question: string,
    rewards: Faction,
    punishes: Faction,
    type: "degree"
}

export type MultiQuestion = Question & {
    question: string,
    individual: string,
    carer: string,
    disruptor: string
    type: "multi"
}

export type Faction = "disruptor" | "carer" | "individual"