import { writable } from "svelte/store"

export const factionScores = writable({
    disruptor:  0,
    carer:      0,
    individual: 0,
})

export const questionLog = writable<string>("")